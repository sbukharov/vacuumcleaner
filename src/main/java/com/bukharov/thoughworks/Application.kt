package com.bukharov.thoughworks

import com.bukharov.thoughworks.cli.CliGame

fun main(args: Array<String>) {
    CliGame(input = System.`in`, output = System.out)
        .start()
}