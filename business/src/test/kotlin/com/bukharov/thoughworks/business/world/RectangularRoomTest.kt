package com.bukharov.thoughworks.business.world

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class RectangularRoomTest {

    @Test
    fun `when width and height are correct room will be created`() {
        val width = 2
        val height = 2

        RectangularRoom(width, height)
        assertTrue(true) // no exceptions happened
    }

    @Test
    fun `when a plate is exist it should be retrieved`() {
        val room = RectangularRoom(2, 2)
        val x = 0
        val y = 0
        val plate = room.getPlate(Coordinate(x, y))

        assertTrue(plate is Plate.Flour)
        plate as Plate.Flour
        assertEquals(x, plate.coordinate.x )
        assertEquals(y, plate.coordinate.y )
    }

    @Test
    fun `when a plate is not exist it should be wall`() {
        val room = RectangularRoom(2, 2)
        val x = 5
        val y = 5

        val plate = room.getPlate(Coordinate(x, y))

        assertTrue(plate is Plate.Wall)
    }

    @Test
    fun `when plate coordinate x is on the border Wall should be returned`() {
        val room = RectangularRoom(2, 2)
        val plate = room.getPlate(Coordinate(2, 0))

        assertTrue(plate is Plate.Wall)
    }

    @Test
    fun `when plate coordinate y is on the border Wall should be returned`() {
        val room = RectangularRoom(2, 2)
        val plate = room.getPlate(Coordinate(0, 2))

        assertTrue(plate is Plate.Wall)
    }

    @Test
    fun `when plate coordinate is negative wall be returned`() {
        val wall = RectangularRoom(2, 2)
            .getPlate(Coordinate(-1, -1))
        assertTrue(wall is Plate.Wall)
    }

    @Test
    fun `when coordinates x near border flour should be returned`() {
        val room = RectangularRoom(2, 2)
        val plate = room.getPlate(Coordinate(1, 0))

        assertTrue(plate is Plate.Flour)
    }

    @Test
    fun `when coordinates y near border flour should be returned`() {
        val room = RectangularRoom(2, 2)
        val plate = room.getPlate(Coordinate(0, 1))

        assertTrue(plate is Plate.Flour)
    }

    @ParameterizedTest
    @MethodSource("notSupportedRoomParams")
    fun `when width of a room is not valid then exception should be thrown`(
        width: Int, height: Int, msg: String) {

        assertThrows(
            IllegalArgumentException::class.java,
            { RectangularRoom(width, height) },
            msg
        )
    }

    companion object {
        @JvmStatic
        fun notSupportedRoomParams() =
            Stream.of(
                Arguments.of(-1, 4, "Width can not be negative"),
                Arguments.of(4, -1, "Height can not be negative"),
                Arguments.of(0, 2, "Width can not be zero"),
                Arguments.of(2, 0, "height can not be zero"),
                Arguments.of(0, 0, "width and height can not be zero")
            )
    }



}