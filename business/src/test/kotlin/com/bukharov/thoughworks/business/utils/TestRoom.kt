package com.bukharov.thoughworks.business.utils

import com.bukharov.thoughworks.business.world.RectangularRoom

val SMALL_ROOM = RectangularRoom(2, 2)
val MEDIUM_ROOM = RectangularRoom(5, 5)