package com.bukharov.thoughworks.business.world

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class PlateTest {

    @ParameterizedTest
    @MethodSource("rightCoordinatesForFlour")
    fun `flour can be created with right coordinates`(x: Int, y: Int, msg: String) {
        val plateCoordinate = Coordinate(x, y)
        val plate = Plate.Flour(plateCoordinate)
        assertEquals(plateCoordinate, plate.coordinate, msg) // no exception has been thrown
    }

    @ParameterizedTest
    @MethodSource("wrongCoordinatesForFlour")
    fun `flour can not be created with wrong coordinates`(x: Int, y: Int, msg: String) {
        assertThrows(
            IllegalArgumentException::class.java,
            { Plate.Flour(Coordinate(x, y)) },
            msg
        )
    }

    companion object {
        @JvmStatic
        fun rightCoordinatesForFlour() = Stream.of(
            Arguments.of(2, 2, "x and y can be small int"),
            Arguments.of(0, 0, "x and y can be 0"),
            Arguments.of(Int.MAX_VALUE, Int.MAX_VALUE, "x and y can be extremely high")
        )

        @JvmStatic
        fun wrongCoordinatesForFlour() = Stream.of(
            Arguments.of(3, -1, "y can not be negative"),
            Arguments.of(-1, 3, "x can not be negative"),
            Arguments.of(Int.MIN_VALUE, Int.MIN_VALUE, "x and y can not be extremely small")
        )
    }

}