package com.bukharov.thoughworks.business.world

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class StateTest {

    @Test
    fun `when state moves forward to North then position increases by Y + 1`() {
        val startingState = State(
            Coordinate.ORIGIN,
            SidesOfTheWorld.North
        )

        val nextState = startingState.moveForward()

        assertEquals(
            State(
                startingState.position.plusOneByY(),
                SidesOfTheWorld.North
            ),
            nextState
        )
    }


    @Test
    fun `when state spines right then direction changes`() {
        val startingState = State(
            Coordinate.ORIGIN,
            SidesOfTheWorld.North
        )
        val finishState = startingState.spinRight()

        assertEquals(SidesOfTheWorld.East, finishState.direction)
    }

    @Test
    fun `when state spines left then direction changes`() {
        val startingState = State(
            Coordinate.ORIGIN,
            SidesOfTheWorld.North
        )
        val finishState = startingState.spinLeft()

        assertEquals(SidesOfTheWorld.West, finishState.direction)
    }

    @Test
    fun `when state spines and moves then position has been changed towards right direction`() {
        val startingPoint = Coordinate.ORIGIN
        val startingState = State(startingPoint, SidesOfTheWorld.North)

        val finishState = startingState
            .spinRight()
            .moveForward()

        assertEquals(
            State(
                Coordinate.ORIGIN.plusOneByX(),
                SidesOfTheWorld.East
            ),
            finishState
        )
    }
}