package com.bukharov.thoughworks.business.command

import com.bukharov.thoughworks.business.world.Cleaner
import com.bukharov.thoughworks.business.world.Coordinate
import com.bukharov.thoughworks.business.world.RectangularRoom
import com.bukharov.thoughworks.business.world.SidesOfTheWorld
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class CommandChainTest {

    @Test
    fun `when all commands executed a cleaner is on expected position`() {
        val cleaner = Cleaner(
            RectangularRoom(6, 6),
            Coordinate(1, 2),
            SidesOfTheWorld.North
        )

        CommandChain()
            .addCommand(SpinLeftCommand())
            .addCommand(MoveForwardOnePlateCommand())
            .addCommand(SpinLeftCommand())
            .addCommand(MoveForwardOnePlateCommand())
            .addCommand(SpinLeftCommand())
            .addCommand(MoveForwardOnePlateCommand())
            .addCommand(SpinLeftCommand())
            .addCommand(MoveForwardOnePlateCommand())
            .addCommand(MoveForwardOnePlateCommand())
            .executeFor(cleaner)

        assertEquals(Coordinate(1, 3), cleaner.position)
        assertEquals(SidesOfTheWorld.North, cleaner.direction)
    }
}