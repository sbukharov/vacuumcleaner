package com.bukharov.thoughworks.business.world

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class SidesOfTheWorldTest {

    @Test
    fun `when rotation is 360 degree right start point the same end point`() {
        assertEquals(
            SidesOfTheWorld.North,
            SidesOfTheWorld.North
                .right()
                .right()
                .right()
                .right()
        )
    }

    @Test
    fun `when rotation is 360 degree left start point the same end point`() {
        assertEquals(
            SidesOfTheWorld.North,
            SidesOfTheWorld.North
                .left()
                .left()
                .left()
                .left()
        )
    }
}