package com.bukharov.thoughworks.business.world

import com.bukharov.thoughworks.business.utils.SMALL_ROOM
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class CleanerTest {

    @Test
    fun `when cleaner put in the room then state is equals to starting state`() {
        val startingPoint = Coordinate.ORIGIN
        val startingDirection = SidesOfTheWorld.North
        val cleaner = Cleaner(SMALL_ROOM, startingPoint, startingDirection)

        assertEquals(startingPoint, cleaner.position)
        assertEquals(startingDirection, cleaner.direction)
    }

    @Test
    fun `when cleaner spines and moves then position has been changed towards right direction`() {
        val startingPoint = Coordinate.ORIGIN
        val cleaner = Cleaner(
            SMALL_ROOM,
            startingPoint,
            SidesOfTheWorld.North
        )

        cleaner.spinRight()
        cleaner.moveForward()

        assertEquals(Coordinate.ORIGIN.plusOneByX(), cleaner.position)
        assertEquals(SidesOfTheWorld.East, cleaner.direction)
    }

    @Test
    fun `when cleaner hit the wall it stops`() {
        val cleaner = Cleaner(
            SMALL_ROOM,
            Coordinate.ORIGIN,
            SidesOfTheWorld.North
        )
        cleaner
            .moveForward()
            .moveForward() // hit the wall

        assertEquals(Coordinate(0, 1), cleaner.position)
    }


    /**
     * RectangularRoom Size: 6 x 6
     * Vacuum Cleaner initial state: (1, 2, N)
     * Instructions: [L, M, L, M, L, M, L, M, M]
     * Final state: (1, 3, N)
     */
    @Test
    fun `when cleaner goes through complicated route from example 1 then final position is right`() {
        val cleaner = Cleaner(
            RectangularRoom(6, 6),
            Coordinate(1, 2),
            SidesOfTheWorld.North
        )
        cleaner
            .spinLeft()
            .moveForward()
            .spinLeft()
            .moveForward()
            .spinLeft()
            .moveForward()
            .spinLeft()
            .moveForward()
            .moveForward()

        assertEquals(Coordinate(1, 3), cleaner.position)
        assertEquals(SidesOfTheWorld.North, cleaner.direction)
    }

    /**
     * RectangularRoom Size: 6 x 6
     * Vacuum Cleaner initial state: (3, 5, N)
     * Instructions: [M, L, M]
     * Final state: (2, 5, W)
     * Note: The first ‘M’ had no effect as the vacuum cleaner bumped into the wall.
     */
    @Test
    fun `when cleaner goes through complicated route from example 2 then final position is right`() {
        val cleaner = Cleaner(
            RectangularRoom(6, 6),
            Coordinate(3, 5),
            SidesOfTheWorld.North
        )

        cleaner
            .moveForward()
            .spinLeft()
            .moveForward()

        assertEquals(Coordinate(2, 5), cleaner.position)
        assertEquals(SidesOfTheWorld.West, cleaner.direction)
    }

    @Test
    fun `when cleaner is outside of the room then exception will be thrown`() {
        assertThrows(IllegalStateException::class.java)
        {
            Cleaner(
                SMALL_ROOM,
                Coordinate(100, 100),
                SidesOfTheWorld.North
            )
        }
    }
}