package com.bukharov.thoughworks.business.command

import com.bukharov.thoughworks.business.Movable

interface Command {
    fun execute(thing: Movable)
}

class MoveForwardOnePlateCommand: Command {
    override fun execute(thing: Movable) {
        thing.moveForward()
    }
}

class SpinLeftCommand: Command {
    override fun execute(thing: Movable) {
        thing.spinLeft()
    }
}

class SpinRightCommand: Command {
    override fun execute(thing: Movable) {
        thing.spinRight()
    }
}