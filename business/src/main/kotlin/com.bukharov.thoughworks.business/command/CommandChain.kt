package com.bukharov.thoughworks.business.command

import com.bukharov.thoughworks.business.Movable
import java.util.*

/**
 * Represents a chain of command for execution
 * Not thread safe
 */
class CommandChain {

    private val commands: MutableList<Command> = LinkedList()

    fun addCommand(newCommand: Command): CommandChain {
        commands.add(newCommand)
        return this
    }

    fun executeFor(thing: Movable) = commands.forEach { it.execute(thing) }
}