package com.bukharov.thoughworks.business.world

import com.bukharov.thoughworks.business.Movable
import mu.KotlinLogging

class Cleaner(
    private val room: RectangularRoom,
    startingPoint: Coordinate,
    direction: SidesOfTheWorld
): Movable {

    private var state = State(startingPoint, direction)

    init {
        // we cant put cleaner out of the room
        room.getPlate(state.position) as? Plate.Flour
            ?: throw IllegalStateException("Cleaner $this position is out of the room $room")
    }

    val position: Coordinate
    get() = state.position

    val direction: SidesOfTheWorld
    get() = state.direction

    override fun moveForward(): Cleaner {
        val nextPossibleState = state.moveForward()

        when (room.getPlate(nextPossibleState.position)) {
            is Plate.Flour -> state = nextPossibleState
            is Plate.Wall -> logger.info { "$this hit the wall" }
        }
        return this
    }

    override fun spinRight(): Cleaner {
        state = state.spinRight()
        return this
    }

    override fun spinLeft(): Cleaner {
        state = state.spinLeft()
        return this
    }

    override fun toString() = "Cleaner at $state in Room $room"

}

private val logger = KotlinLogging.logger {  }
