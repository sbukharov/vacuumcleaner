package com.bukharov.thoughworks.business.world

/**
 * Represents current or potential state of something.
 * Threadsafe because of immutable
 */
data class State(
    val position: Coordinate,
    val direction: SidesOfTheWorld
) {

    fun moveForward(): State {
        val nextPosition = when(direction) {
            is SidesOfTheWorld.North -> position.plusOneByY()
            is SidesOfTheWorld.South -> position.minusOneByY()
            is SidesOfTheWorld.West -> position.minusOneByX()
            is SidesOfTheWorld.East -> position.plusOneByX()
        }
        return copy(position = nextPosition)
    }

    fun spinLeft() = copy(direction = direction.left())
    fun spinRight() = copy(direction = direction.right())
}