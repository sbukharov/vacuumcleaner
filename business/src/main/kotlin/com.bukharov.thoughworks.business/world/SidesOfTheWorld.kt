package com.bukharov.thoughworks.business.world


sealed class SidesOfTheWorld {

    abstract fun right(): SidesOfTheWorld
    abstract fun left(): SidesOfTheWorld

    override fun toString(): String {
        return this::class.simpleName?:"UNKNOWN DIRECTION"
    }

    object North: SidesOfTheWorld() {
        override fun right() = East
        override fun left() = West
    }

    object South: SidesOfTheWorld(){
        override fun right() = West
        override fun left() = East
    }
    object West: SidesOfTheWorld() {
        override fun right() = North
        override fun left() = South
    }
    object East: SidesOfTheWorld() {
        override fun right() = South
        override fun left() = North
    }
}