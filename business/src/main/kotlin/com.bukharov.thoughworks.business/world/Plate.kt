package com.bukharov.thoughworks.business.world

sealed class Plate {
    data class Flour(val coordinate: Coordinate): Plate() {
        init {
            if (coordinate.x < 0 || coordinate.y < 0 ) {
                throw IllegalArgumentException(
                    "Flour coordinate should be positive. Given: $coordinate")
            }
        }
    }
    object Wall: Plate()
}
