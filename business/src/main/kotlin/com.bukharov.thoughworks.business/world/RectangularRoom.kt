package com.bukharov.thoughworks.business.world

/**
 * Represents simple rectangle room with size X*Y
 * It does not contain any state of plates therefore it is not possible to use this romm
 * with several cleaners
 *
 * Every time when getPlate calls will be returned different plates instance
 */
data class RectangularRoom(
    private val width: Int,
    private val height: Int
) {

    init {
        if (width <= 0) throw IllegalArgumentException("RectangularRoom width should be > 0. $width given")
        if (height <= 0) throw IllegalArgumentException("RectangularRoom height should be > 0. $height given")
    }

    fun getPlate(coordinate: Coordinate): Plate {

        if (coordinate.x < 0 || coordinate.y < 0) {
            return Plate.Wall
        }

        if (coordinate.x >= width || coordinate.y >= height) {
            return Plate.Wall
        }

        return Plate.Flour(coordinate)
    }
}