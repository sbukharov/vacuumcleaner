package com.bukharov.thoughworks.business.world

/**
 * Represent a point in a Cartesian coordinate system
 *
 * Coordinates can be negative
 * Coordinates are thread safe because of immutable
 */
data class Coordinate(
    val x: Int,
    val y: Int
) {
    companion object {
        val ORIGIN = Coordinate(0, 0)
    }

    fun plusOneByX(): Coordinate = copy(x = x + 1)
    fun plusOneByY(): Coordinate = copy(y = y + 1)

    fun minusOneByX(): Coordinate = copy(x = x - 1)
    fun minusOneByY(): Coordinate = copy(y = y - 1)
}