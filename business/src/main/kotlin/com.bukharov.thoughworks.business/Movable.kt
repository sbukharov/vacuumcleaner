package com.bukharov.thoughworks.business

/**
 * You can move object that implements this interface forward, spin it left and spin it right
 */
interface Movable {
    fun moveForward(): Movable
    fun spinLeft(): Movable
    fun spinRight(): Movable
}