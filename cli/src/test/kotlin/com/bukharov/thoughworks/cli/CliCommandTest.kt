package com.bukharov.thoughworks.cli

import com.bukharov.thoughworks.business.Movable
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class CliCommandTest {

    @ParameterizedTest
    @MethodSource("rightCommands")
    fun `when cli command argument is right then expected command should be executed`(
        commandLiteral: Char,
        expectedExecutedCommand: SimpleMovable.ExecutedCommand
    ) {
        val movable = SimpleMovable()
        CliCommand(commandLiteral).execute(movable)
        assertEquals(
            expectedExecutedCommand,
            movable.lastExecutedCommand
        )
    }
    companion object {
        @JvmStatic
        fun rightCommands() = Stream.of(
            Arguments.of('M', SimpleMovable.ExecutedCommand.FORWARD),
            Arguments.of('L', SimpleMovable.ExecutedCommand.LEFT),
            Arguments.of('R', SimpleMovable.ExecutedCommand.RIGHT)
        )
    }

    @Test
    fun `when cli command literal is not supported then exception should be thrown`() {
        val movable = SimpleMovable()
        assertThrows(
            IllegalArgumentException::class.java,
            { CliCommand('Q').execute(movable) },
            "Current literal should not be supported"
        )
    }


}

/**
 * Just for testing commands
 */
internal class SimpleMovable: Movable {

    var lastExecutedCommand: ExecutedCommand? = null

    override fun moveForward(): Movable {
        lastExecutedCommand = ExecutedCommand.FORWARD
        return this
    }

    override fun spinLeft(): Movable {
        lastExecutedCommand = ExecutedCommand.LEFT
        return this
    }

    override fun spinRight(): Movable {
        lastExecutedCommand = ExecutedCommand.RIGHT
        return this
    }

    enum class ExecutedCommand {
        FORWARD, LEFT, RIGHT
    }

}