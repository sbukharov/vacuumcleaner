package com.bukharov.thoughworks.cli

import com.bukharov.thoughworks.business.Movable
import com.bukharov.thoughworks.business.command.Command
import com.bukharov.thoughworks.business.command.MoveForwardOnePlateCommand
import com.bukharov.thoughworks.business.command.SpinLeftCommand
import com.bukharov.thoughworks.business.command.SpinRightCommand

class CliCommand(literal: Char): Command {

    private val wrappedCommand: Command = when (literal) {
        'M' -> MoveForwardOnePlateCommand()
        'L' -> SpinLeftCommand()
        'R' -> SpinRightCommand()
        else -> throw IllegalArgumentException(
            "Command $literal does not supported. Supported commands: ${supportedCommands()}")
    }

    override fun execute(thing: Movable) {
        wrappedCommand.execute(thing)
    }

    companion object {
        fun supportedCommands() = setOf('M', 'L', 'R')
    }

}