package com.bukharov.thoughworks.cli

import java.io.PrintStream
import java.util.*

class InputFriendlyScanner(
    private val scanner: Scanner,
    private val output: PrintStream
) {

    fun askToInputInt(humanMessage: String, defaultValue: Int): Int {
        output.println("$humanMessage. Default value [$defaultValue]" )
        val inputIsCorrect = false
        while (!inputIsCorrect) {
            val line = scanner.nextLine()

            if (line.isEmpty()) {
                return defaultValue
            }

            val potentialInt = line.toIntOrNull()
            if (potentialInt != null) {
                return potentialInt
            }

            output.println("please try again. Input should be an integer")
        }

        return defaultValue
    }

    fun askToInputChar(humanMessage: String, possibleChars: Set<Char>): Char {
        val defaultValue = possibleChars.first()
        output.println("$humanMessage. Default value [${possibleChars.first()}]")
        output.println("Possible chars are: ${possibleChars.joinToString()}")

        val inputIsCorrect = false
        while (!inputIsCorrect) {
            val line = scanner.nextLine()

            if (line.isEmpty()) {
                return defaultValue
            }

            val potentialChar = line.toCharArray()
            if (potentialChar.size > 1) {
                output.println("too many chars. Possible only one")
                continue
            }

            if (potentialChar.first().toUpperCase() in possibleChars) {
                return potentialChar.first().toUpperCase()
            } else {
                output.println("${potentialChar.first()} is not possible. " +
                        "Possible Values: ${possibleChars.joinToString()}")
                continue
            }
        }

        return defaultValue
    }

    fun print(msg: String) {
        output.println(msg)
    }
}