package com.bukharov.thoughworks.cli

import com.bukharov.thoughworks.business.world.SidesOfTheWorld

object CliCompass {

    private val sides: Map<Char, SidesOfTheWorld> = mapOf(
        'N' to SidesOfTheWorld.North,
        'E' to SidesOfTheWorld.East,
        'W' to SidesOfTheWorld.West,
        'S' to SidesOfTheWorld.South
    )

    fun getSideOfTheWorld(char: Char): SidesOfTheWorld {
        return sides[char]?:throw IllegalArgumentException("$char is not a side of the world")
    }

    fun supportedChars() = sides.keys
}