package com.bukharov.thoughworks.cli

import com.bukharov.thoughworks.business.command.CommandChain
import com.bukharov.thoughworks.business.world.Cleaner
import com.bukharov.thoughworks.business.world.Coordinate
import com.bukharov.thoughworks.business.world.RectangularRoom
import java.io.InputStream
import java.io.PrintStream
import java.util.*

class CliGame(
    private val input: InputStream,
    private val output: PrintStream
) {
    fun start() {
        val scanner = InputFriendlyScanner(Scanner(input), output)

        val room = createRoom(scanner)
        val cleaner = setUpCleaner(scanner, room)
        val commands = obtainCommands(scanner)

        commands.executeFor(cleaner)
        scanner.print("Cleaner now at ${cleaner.position} direction ${cleaner.direction}")
    }

    private fun obtainCommands(scanner: InputFriendlyScanner): CommandChain {
        val commands = CommandChain()
        var wasLastCommand = false
        while (!wasLastCommand) {
            val possibleChars = CliCommand.supportedCommands().plus('X')
            val commandChar = scanner.askToInputChar(
                "please input command for the cleaner. Or Type X if it is your last command",
                possibleChars
            )

            if (commandChar.toUpperCase() == 'X') {
                wasLastCommand = true
            } else {
                commands.addCommand(CliCommand(commandChar))
            }
        }
        return commands
    }

    private fun setUpCleaner(
        scanner: InputFriendlyScanner,
        room: RectangularRoom
    ): Cleaner {
        val cleanerStartPointX = scanner.askToInputInt("please input cleaner X start point", 0)
        val cleanerStartPointY = scanner.askToInputInt("please input cleaner Y start point", 0)
        val directionChar = scanner.askToInputChar("please specify cleaner's direction", CliCompass.supportedChars())
        return Cleaner(
            room,
            Coordinate(cleanerStartPointX, cleanerStartPointY),
            CliCompass.getSideOfTheWorld(directionChar)
        )
    }

    private fun createRoom(scanner: InputFriendlyScanner): RectangularRoom {
        val roomWidth = scanner.askToInputInt("Please input room width", 5)
        val roomHeight = scanner.askToInputInt("Please input room height", 5)
        return RectangularRoom(roomWidth, roomHeight)
    }
}